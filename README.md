### Pre-work and Notes for MERIF attendees

- The MERIF 2023 tutorial session is very much hands-on. Please bring your own laptop with charger.

- For the session we will make use of browser-based VNC remote desktop functionality. Based on our experience this works best with the Google Chrome browser. Please have Chrome installed on your laptop ahead of time. 

- We will make use of the POWDER "merif2023" project. Please join the project before the start of MERIF:

    - If you already have an account on POWDER:

        - Log into [https://www.powderwireless.net](https://www.powderwireless.net) 
        - Click on your name in the top right and select "Start/Join Project"
        - Select "Join Existing Project" and enter: merif2023
        - Click on "Submit Request"

    - If you do not have a POWDER account:

        - Go to [https://www.powderwireless.net](https://www.powderwireless.net)
        - Click on "Request an Account"
        - Complete the details on the left
        - On the right select "Join Existing Project" and enter: merif2023 
        - Click on "Submit Request"

    - If you have a GENI account, you will not be able to use that account to join a new project.  The easiest thing to is to create a new account, but you can also request an account migration.  To request an account migration, please email powder-support@powderwireless.net and include your GENI user id.

    - POWDER staff will process these requests before the start of MERIF insofar as possible, or as the tutorial is beginning.

    - Please wait to run POWDER experiments prior to the tutorial session.

- During MERIF, several POWDER team members will be present during hands-on sessions so you can ask them directly when you get stuck.

- We have also set up a Discord server for tutorial attendees where you can also ask for help.  Use this invite link [https://discord.gg/ynda57Qv](https://discord.gg/ynda57Qv) to get started.

  * You can use the discord server to report issues with account creation, project join requests, or with your O-RAN experiment.


### MERIF 2023 - O-RAN Session

- Hands-on: [O-RAN - Near RT RIC](https://gitlab.flux.utah.edu/powderrenewpublic/merif2023/-/blob/main/content/oran.md) (*David Johnson*)

  * Presentation: [NexRAN and Open RAN Overview](https://docs.google.com/presentation/d/14kZsj2XYvJ7CvNUHaT9N-n2sR7IGtZc2SvhUhSHIhVM/edit?usp=sharing)(*David Johnson*)

- ~~Hands-on: [O-RAN - "top-to-bottom" stack change](https://gitlab.flux.utah.edu/powderrenewpublic/merif2023/-/blob/main/content/oran_modify.md) (*David Johnson*)~~

  * *Note:* we will not cover this second session (modifying NexRAN) at MERIF 2023, but if you are interested in modifying NexRAN or our srslte fork, it has useful supplementary content.


### MERIF 2023 - Education Session

- [Presentation](https://docs.google.com/presentation/d/1zyCHmNQNY1W0k8YxDDG5GRVgkmVGFm0X13QXgH7yxnc/edit?usp=sharing)

- Demo: [Teaching 5G with POWDER and OpenAirInterface5G](https://gitlab.flux.utah.edu/powderrenewpublic/merif2023/-/blob/main/content/teaching-5g-oai.md) (*Dustin Maas*)
